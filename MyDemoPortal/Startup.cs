﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MyDemoPortal.Startup))]
namespace MyDemoPortal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
